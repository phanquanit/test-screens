== README

Things you may want to run some command first:

* Installation some gems: bundle install

* Database creation: rake db:create

* Database initialization: rake db:migrate
