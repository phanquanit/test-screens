$(document).ready(function() {
  $(".btn-account").click(function() {
    var parents = $(this).parents('.login-area');
    parents.toggleClass('open');
  });
  $(".menu-inside ~ .main-header .explore").mouseleave(function(){
    if ($(this).val() != '') {
      $(".menu-inside ~ .main-header .explore .submenu").css('display', 'block');
    } else {
      $(".menu-inside ~ .main-header .explore .submenu").removeClass('open');
    }
  });
  $(".explore .search input").click(function() {
    var parents = $(this).parents('.search');
    parents.toggleClass('open');
  });
  $("#nav-icon1").click(function() {
    $(this).toggleClass('open');
    var parents = $(this).parents('.menu-inside.mobile');
    parents.toggleClass('open');
  });
  $(".menu-inside.mobile > ul > li").click(function() {
    $(this).toggleClass('open');
  });

  $("input#explore-search").autocomplete({
    source: ["Toranto", "Colorado", "Ha Noi", "Ho Chi Minh", "California", "Los Angeles", "Da Nang"]
  });
});