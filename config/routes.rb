Rails.application.routes.draw do
  devise_for :users
  devise_scope :users do
    get 'sign_in', to: 'devise/sessions#new'
    get 'sign_up', to: 'devise/registrations#new'
  end

  root 'home#index'
  get '/index2', to: 'home#index2'
  get '/index3', to: 'home#index3'
  get '/index4', to: 'home#index4'
end
