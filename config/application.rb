require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module TestScreens
  class Application < Rails::Application
    config.active_record.raise_in_transactional_callbacks = true
    env_file = Rails.root.join('environments', ".#{Rails.env}.env")
    Dotenv.load(env_file)
    config.assets.paths << Rails.root.join("app", "assets", "fonts")
  end
end
